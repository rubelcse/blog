@extends('layout')

@section('content')
    <div class="col-sm-8 blog-main">
        <h2>Sign in</h2>
       <form action="/login" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label for="">Email</label>
                <input type="email" name="email" class="form-control" id="email">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password">
            </div>
            <div class="form-group">
               <button type="submit" name="login" class="btn btn-primary">Login</button>
            </div>
        </form>
        @include('errors.error')
    </div>
@endsection