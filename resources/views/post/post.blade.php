<div class="blog-post">
    <h2 class="blog-post-title">
        <a href="/show/post/{{$post->id}}">
            {{$post->title}}
        </a>
    </h2>
    <p class="blog-post-meta">
        @if(isset($post->user->name))
        {{$post->User->name}} on
        @endif
        {{$post->created_at->toFormattedDateString()}}
    </p>

    {{$post->body}}
</div><!-- /.blog-post -->