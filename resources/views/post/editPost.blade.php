@extends('layout')

@section('content')
    <div class="col-sm-8 blog-main">

        <h2>Update Your Post</h2>

        <form class="form-horizontal" method="post" action="{{url('/update/post')}}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" value="{{$post->title}}" name="title" id="title" placeholder="economics">
                    <input type="hidden" class="form-control" value="{{$post->id}}" name="id" id="title" placeholder="economics">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Body</label>
                <div class="col-sm-10">
                    <textarea name="body" class="form-control" id="body" rows="5">{{$post->body}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success">Published</button>
                </div>
            </div>
            @include('errors.error')
        </form>



    </div>

@endsection