@extends('layout')

@section('content')
    <div class="col-sm-8 blog-main">
        <h2>{{$post->title}}</h2>

        {{$post->body}}
        @if(auth()->id() == $post->user_id)
            <div class="btn-group">
                <a href="{{url('/post/edit/'.$post->id)}}" title="edit" class="btn btn-success">
                    <span class="glyphicon glyphicon-info-sign">Edit</span>
                </a>
                <a href="{{url('/post/delete/'.$post->id)}}" title="delete" class="btn btn-danger">
                    <span class="glyphicon glyphicon-edit">Delete</span>
                </a>
            </div>
        @endif

        <hr>
        @if(\Illuminate\Support\Facades\Auth::check())
            <div>
        <h2>Comments Below....</h2>
            <form action="/store/like" method="post" class="form-inline">
                {{ csrf_field() }}
                <div class="form-inline" style="width: 50%">
                    <input type="hidden" name="post_id" class="form-control" value="{{$post->id}}">
                    <button type="submit" class="btn btn-success" value="1" name="like">Like</button>
                </div>
            </form>
            </div>
        <strong>Total comments: </strong>{{$post->countComments()}}
        <strong>Total Like: </strong>{{$post->countLikes()}}
        <div class="comments">
            <ul class="list-group">
                @foreach($post->comments as $comment)
                    <li class="list-group-item">
                        @if($comment->parent_id == 0)
                            <strong>
                                @if(count($comment->created_at))
                                    {{$comment->user->name}} on
                                    {{$comment->created_at->diffForHumans()}}: &nbsp;
                                @endif
                            </strong>
                            {{$comment->body}}
                            @if(auth()->id() == $comment->user_id)
                                <div class="btn-group">
                                    <a href="{{url('/comment/edit/'.$comment->id)}}" title="edit" class="btn btn-success">
                                        <span class="glyphicon glyphicon-info-sign">Edit</span>
                                    </a>
                                    <a href="{{url('/comment/delete/'.$comment->id)}}" title="delete" class="btn btn-danger">
                                        <span class="glyphicon glyphicon-edit">Delete</span>
                                    </a>
                                </div>
                            @endif
                        @endif
                    </li>
                        @if($comment->replies)
                            <ul class="list-group">
                            @foreach($comment->replies as $reply)
                                <li class="list-group-item">

                                        @if(count($reply->created_at))
                                            {{$reply->user->name}} on
                                            {{$reply->created_at->diffForHumans()}}: &nbsp;
                                        @endif

                                    {{$reply->body}}
                                </li>
                            @endforeach
                        </ul>
                        @endif
                    @if($comment->parent_id == 0)
                    <div class="card">
                        <div class="card-block">
                            <form action="/comments/replay/{{$comment->id}}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <textarea name="body" class="form-control" placeholder="replay here...."></textarea>
                                    <input type="hidden" name="id" class="form-control" value="{{$post->id}}">
                                    <input type="hidden" name="parent_id" class="form-control" value="{{$comment->id}}">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" name="submit">Replay</button>
                                </div>
                            </form>
                            @include('errors.error')
                        </div>
                    </div>
                    @endif
                    {{--reply comment--}}
                @endforeach

            </ul>
        </div>
        {{--add a comments--}}
        <hr>
        <div class="card">
            <div class="card-block">
                <form action="/post/comments/{{$post->id}}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <textarea name="body" class="form-control" placeholder="Enter your Comments...."></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="submit">Add Comment</button>

                    </div>
                </form>
                @include('errors.error')
            </div>
        </div>
            @endif
    </div>



    @endsection