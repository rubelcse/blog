@extends('layout')

@section('content')
    <div class="col-sm-8 blog-main">
        <h2></h2>

        <div class="card">
            <div class="card-block">
                <form action="/update/comment" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <textarea name="body" class="form-control" placeholder="Enter your Comments....">{{$comment->body}}</textarea>
                        <input type="hidden" name="id" value="{{$comment->id}}">
                        <input type="hidden" name="post_id" value="{{$comment->post_id}}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="submit">Update Comment</button>

                    </div>
                </form>
                @include('errors.error')
            </div>
        </div>

    </div>

    @endsection