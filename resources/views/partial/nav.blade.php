<div class="blog-masthead">
    <div class="container">
        <nav class="nav blog-nav">
            <a class="nav-link active" href="{{url('/')}}">Home</a>
            @if(\Illuminate\Support\Facades\Auth::check())
                <a class="nav-link" href="{{url('/create/post')}}">Create Post</a>
                <a class="nav-link ml-auto" href="/logout">{{\Illuminate\Support\Facades\Auth::user()->name}} Logout</a>
                @else
                <a class="nav-link" href="{{url('/register')}}">Register</a>
                <a class="nav-link" href="{{url('/login/create')}}">Login</a>
            @endif
        </nav>
    </div>
</div>