<!DOCTYPE html>
<html lang="en">
<head>
   @include('partial.title')
</head>

<body>

@include('partial.nav')

@include('partial.header')
<div class="container">

    <div class="row">

@yield('content')


@include('partial.headerSidebar')

    </div><!-- /.row -->

</div><!-- /.container -->

@include('partial.footer')

</body>
</html>
