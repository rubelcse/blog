<?php

//Manage post
Route::get('/','PostsController@index')->name('home');
Route::get('/create/post','PostsController@createPost');
Route::post('/store/post','PostsController@storePost');
Route::get('/show/post/{post}','PostsController@showPost');
Route::get('/post/edit/{post}','PostsController@editPost');
Route::post('/update/post','PostsController@updatePost');
Route::get('/post/delete/{post}','PostsController@deletePost');
//manage comments
Route::post('/post/comments/{post}','CommentsController@storeComments');
Route::get('/comment/edit/{id}','CommentsController@editComments');
Route::post('/update/comment','CommentsController@updateComments');
Route::get('/comment/delete/{id}','CommentsController@deleteComments');
//manage Like
Route::post('/store/like','LikeController@storeLike');
//manage replay
Route::post('/comments/replay/{comment}','CommentsController@storeReplay');
//manage registration
Route::get('/register','RegistrationController@create');
Route::post('/register/store','RegistrationController@store');
//manage login and logout
Route::get('/login/create','SessionController@create');
Route::post('/login','SessionController@store');
Route::get('/logout','SessionController@destroy');