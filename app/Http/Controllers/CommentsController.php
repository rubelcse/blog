<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function storeComments(Post $post)
    {
        $this->validate(\request(),[
            'body' => 'required|min:2',
        ]);
      $post->addComments(\request('body'));
      return back();
    }

    public function storeReplay(Comment $comment)
    {
        $this->validate(\request(),[
           'body' => 'required|min:2',
        ]);
        $comment->addReply(\request('body'));
        return back();
    }

    public function editComments($id)
    {
        $comment = Comment::where('id',$id)->first();
        return view('comment.editComments',compact('comment'));
    }

    public function updateComments(Request $request,Post $post)
    {
        $comment = Comment::find($request->id);
        $comment->body = $request->body;
        $comment->user_id = auth()->id();
        $comment->post_id = $request->post_id;
        $comment->save();
        return redirect('/show/post/'.$request->post_id);

    }

    public function deleteComments($id,Comment $comment)
    {
       $comment = Comment::find($id);
       $comment->replies()->delete();
       $comment->delete();
       return redirect()->back();

    }
}
