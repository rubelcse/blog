<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Carbon\Carbon;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','showPost']]);
    }

    public function index()
    {
        $posts = Post::latest()
            ->filter(\request(['month','year']))
            ->get();

        //return $archives;
        return view('post.index',compact('posts'));
    }

    public function showPost(Post $post)
    {
        //find all of post using orm model..route passing variable and argument type hinting must be same
        return view('post.showPost',compact('post'));
    }

    public function createPost()
    {
        return view('post.createPost');
    }

    public function storePost()
    {
        $this->validate(\request(),[
            'title' => 'required',
            'body' => 'required'
        ]);
       auth()->user()->publish(
           new Post(\request(['title','body'])
           ));
        return redirect('/');
    }
    public function editPost($id)
    {
        $post = Post::where('id',$id)->first();
        return view('post.editPost',compact('post'));
    }

    public function updatePost(Request $request)
    {
        $post = Post::find($request->id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->user_id = auth()->id();
        $post->save();
        return redirect('/show/post/'.$request->id);
    }

    public function deletePost($id)
    {
        $post = Post::find($id);
        //$post->comments()->replies()->delete();
        $post->comments()->delete();
        $post->delete();
        return redirect('/');
    }

}
