<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    public function create()
    {
        return view('registration.create');
    }

    public function store()
    {
        //validation her
        $this->validate(\request(),[
           'name' => 'required',
            'email' => 'required|email',
           'password' => 'required|confirmed',
        ]);
        $data = \request(['email','name']);
        $data['password'] = bcrypt(\request('password'));
        /*echo "<pre>";
        print_r($data);die();*/
        $user = User::create($data);
        \auth()->login($user);
        return redirect()->home();
    }
}
