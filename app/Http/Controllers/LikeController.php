<?php

namespace App\Http\Controllers;

use App\Like;
use App\Post;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function storeLike(Request $request)
    {
        $this->validate($request,[
            'like' => 'required',
        ]);
        //dd($request->all());
        $like = new Like();
        $like->user_id = auth()->id();
        $like->post_id = $request->post_id;
        $like->like = $request->like;
        $like->save();
        return redirect()->back();

    }
}
