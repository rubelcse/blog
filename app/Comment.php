<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['body','post_id','user_id','parent_id'];
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function replies() {
        return $this->hasMany('App\Comment', 'parent_id');
    }
    public function addReply($body)
    {
        Comment::create([
            'body' => \request('body'),
            'post_id' => request('id'),
            'user_id' => auth()->id(),
            'parent_id' => request('parent_id'),
        ]);

    }
}
