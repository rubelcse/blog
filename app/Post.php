<?php

namespace App;
use App\Comment;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title','body','user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'post_id');
    }
    public function countComments()
    {
        return $this->comments()->count();
    }
    public function likes()
    {
        return $this->hasMany(Like::class,'post_id');
    }
    public function countLikes()
    {
        return $this->likes()->count();
    }

    public function addComments($body)
    {
          Comment::create([
              'body' => \request('body'),
              'post_id' => $this->id,
              'user_id' => auth()->id(),
         ]);
       // $this->comments()->create(compact('body'));

    }
    public function scopeFilter($queary,$filters)
    {
        if ($month = $filters['month']){
            $queary->whereMonth('created_at',Carbon::parse($month)->month);
        }
        if ($year = $filters['year']){
            $queary->whereYear('created_at',$year);
        }
    }

    public static function archives()
    {
        return static::selectRaw('year(created_at) year,monthname(created_at) month,count(*) published')
         ->groupBy('year','month')
         ->orderByRaw('min(created_at) desc')
         ->get();
    }
}
